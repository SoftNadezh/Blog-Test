#/bin/bash
#Paquetes Necesarios para compilar Asterisk T-Contacta

echo "#############################"
echo "# Creando Repositorio local #"
echo "#############################"

cd
setenforce 0
rm -rf /etc/selinux/config
cp /usr/src/0Dependencias/config /etc/selinux/
/etc/init.d/iptables stop
chkconfig iptables off
cd /usr/src/0Dependencias/
rpm -ivh /usr/src/0Dependencias/rhel6.5/deltarpm-*
rpm -ivh /usr/src/0Dependencias/rhel6.5/python-deltarpm-*
rpm -ivh /usr/src/0Dependencias/rhel6.5/createrepo-*
createrepo /usr/src/0Dependencias/rhel6.5/
rm -rf /etc/yum.repos.d/*.repo
cp /usr/src/0Dependencias/rhel_local_65.repo /etc/yum.repos.d/
yum clean all
yum list

echo "###########################"
echo "# Instalando Dependencias #"
echo "###########################"

yum -y install compat-glibc-headers headers httpd*
yum -y install kernel kernel-debug kernel-debug-devel kernel-doc grub
yum -y install ntp subversion
yum -y install libvorbis libvorbis-devel vorbis-tools libogg libogg-devel
yum -y install curl curl-devel libidn-devel
yum -y install gcc ncurses-devel make* qt-odbc gcc-c++ compat-libtermcap zlib-devel libtool libtool-ltdl
yum -y install bison bison-devel openssl-devel bzip2-devel slang slang-devel wget newt-devel flex gtk2-devel
yum -y install unixODBC unixODBC-devel libtool-ltdl-devel
yum -y install festival festival-devel
yum -y install kernel-devel
yum -y install libtiff libtiff-devel libxml2 libxml2-devel
yum -y install gnutls gnutls-devel gnutls-utils
yum -y install procmail sendmail sendmail-milter sendmail-devel sendmail-cf
yum -y install compat-openldap openldap openldap-clients openldap-devel openldap-servers
yum -y install net-snmp net-snmp-devel net-snmp-libs net-snmp-utils
yum -y install readline-devel
yum -y install perl perl-libwww-perl
yum -y install ghostscript ghostscript-devel sharutils ghostscript-fonts
yum -y install pam-devel speex speex-devel
yum -y install dhcp* php*
yum -y install git logrotate
yum -y install postgresql-*
yum -y install lua lua-devel libibverbs librdmacm corosync corosynclib radiusclient-ng libical sqlite2 sqlite2-devel
yum -y install neon neon-devel libical-devel radiusclient-ng-devel corosynclib-devel jack-audio-connection-kit-devel
yum -y install gsm gsm-devel libuuid-devel corosynclib-devel
yum -y install spandsp spandsp-devel
yum -y install libedit libedit-devel
yum -y install portaudio portaudio-devel
yum -y install freetds freetds-devel
yum -y install sqlite sqlite-devel
yum -y install libiksemel3 iksemel* mariadb*
yum -y install automake bluez-libs-devel bzip2 corosynclib-devel gcc gcc-c++ git gsm-devel jansson-devel libcurl-devel 
yum -y install libedit-devel libical-devel libogg-devel libtool-ltdl-devel libuuid-devel libvorbis-devel libxml2-devel 
yum -y install libxslt-devel lua-devel mariadb-devel nano ncurses-devel neon-devel net-snmp-devel newt-devel openldap-devel 
yum -y install openssl-devel perl popt-devel postgresql-devel speex-devel sqlite-devel subversion unixODBC-devel uuid-devel wget
yum -y install heartbeat cluster-glue resource-agents pacemaker
yum -y install sshpass*

echo "#################################################"
echo "# Copiando Dependencias Dahdi, Asterisk, Libpri #"
echo "# Instalando UnixODBC                           #"
echo "#################################################"

php -q /usr/src/0Dependencias/hostname.php >> /etc/hosts
mkdir /var/www/html/dahdi/
cp /usr/src/0Dependencias/dahdi/* /var/www/html/dahdi/
cp /usr/src/0Dependencias/certified-asterisk-11.6-current.tar.gz /usr/src/
cp /usr/src/0Dependencias/dahdi-linux-complete-2.10.0.1+2.10.0.1.tar.gz /usr/src/
cp /usr/src/0Dependencias/libpri-1.4-current.tar.gz /usr/src/
cp /usr/src/0Dependencias/unixODBC-2.2.14.tar.gz /usr/src/
cp /usr/src/0Dependencias/wanpipe-7.0.20.tgz /usr/src/
/etc/init.d/httpd start
chkconfig httpd on

cd /usr/src/
tar -xvzf /usr/src/libpri-1.4-current.tar.gz
tar -xvzf /usr/src/dahdi-linux-complete-2.10.0.1+2.10.0.1.tar.gz
tar -xvzf /usr/src/certified-asterisk-11.6-current.tar.gz
tar -xvzf /usr/src/unixODBC-2.2.14.tar.gz
cd /usr/src/unixODBC-2.2.14
./configure --sysconfdir=/etc --enable-gui=no
make
make install
reboot
