#/bin/bash
#Paquetes Necesarios para compilar Asterisk T-Contacta

echo "####################"
echo "# Instalando Dahdi #"
echo "####################"

echo "######## Coloca la ruta de los paquetes dahdi-local  ##########"
echo "######## en FIRMWARE_URL:http://IP-SERVIDOR/dahdi/   ##########"

cd /usr/src/dahdi-linux-complete-2.10.0.1+2.10.0.1/
make clean
make
make install
make config

echo "#####################"
echo "# Instalando Libpri #"
echo "#####################"

cd /usr/src/libpri-1.4.15/
make clean
make
make install

echo "#######################"
echo "# Instalando Asterisk #"
echo "#######################"

cd /usr/src/certified-asterisk-11.6-cert9
./configure --libdir=/usr/lib64
make menuselect.makeopts
menuselect/menuselect --enable res_config_mysql --enable app_mysql --enable cdr_mysql --disable format_mp3 --enable app_chanisavail --enable  app_fax --enable app_dahdiras --enable app_dictate --enable app_fax --enable app_festival --enable cdr_odbc --enable app_saycountpl --enable app_ivrdemo --enable app_saycounted --enable app_readfile --enable app_setcallerid --enable res_fax --enable res_fax_spandsp --enable res_http_websocket --enable res_phoneprov --enable astman --enable conf2ael --enable muted --enable cdr_pgsql --enable chan_console --enable pbx_realtime --enable res_config_pgsql --enable res_fax_spandsp --enable res_snmp --disable CORE-SOUNDS-EN-GSM --disable MOH-OPSOUND-WAV menuselect.makeopts
menuselect/menuselect --enable app_chanisavail --enable app_dahdiras --enable app_festival --enable app_image --enable app_meetme --enable app_saycounted --enable cdr_odbc --enable cdr_pgsql --enable chan_console --enable  pbx_realtime --enable res_fax --enable res_config_pgsql --enable res_fax_spandsp --enable res_snmp
make
make install
make samples
make config
cd
service asterisk start
chkconfig asterisk on

echo "###################"
echo "# Instalando Java #"
echo "###################"

rpm -ivh /usr/src/1Java_Tomcat/jdk-7u65-linux-x64.rpm
rm -rf /etc/profile
cp /usr/src/1Java_Tomcat/profile /etc/
chmod 777 /etc/profile

cp /usr/src/1Java_Tomcat/apache-tomcat-7.0.28.tar.gz /usr/local/
cd /usr/local/
tar -xvzf apache-tomcat-7.0.28.tar.gz
cd
cp /usr/src/1Java_Tomcat/TContactaUno.war /usr/local/apache-tomcat-7.0.28/webapps/
chmod 777 -R /usr/local/apache-tomcat-7.0.28/*
chmod 777 /usr/local/apache-tomcat-7.0.28
sh /usr/local/apache-tomcat-7.0.28/bin/startup.sh
cd
/etc/init.d/tomcat60 start
rm -rf /usr/local/apache-tomcat-7.0.28/conf/tomcat-users.xml
cp /usr/src/1Java_Tomcat/tomcat-users.xml /usr/local/apache-tomcat-7.0.28/conf/
cp /usr/src/1Java_Tomcat/tomcat60 /etc/init.d/
chmod 777 /etc/init.d/tomcat60
ln -s /etc/init.d/tomcat60 /etc/rc2.d/S90tomcat
ln -s /etc/init.d/tomcat60 /etc/rc3.d/S90tomcat
ln -s /etc/init.d/tomcat60 /etc/rc4.d/S90tomcat
ln -s /etc/init.d/tomcat60 /etc/rc5.d/S90tomcat

/etc/init.d/httpd restart
/etc/init.d/tomcat60 stop
/etc/init.d/tomcat60 start
service postgresql initdb
/etc/init.d/postgresql start
chkconfig postgresql on

rm -rf /var/lib/pgsql/data/pg_hba.conf
cp /usr/src/1Java_Tomcat/pg_hba.conf  /var/lib/pgsql/data/pg_hba.conf
rm -rf /var/lib/pgsql/data/postgresql.conf
cp /usr/src/1Java_Tomcat/postgresql.conf /var/lib/pgsql/data/postgresql.conf
/etc/init.d/postgresql restart

cd
su -c "psql -c \" create role tcontacta  with  login encrypted password 'tcontacta' superuser inherit createdb createrole\"" postgres
su - postgres '-c createdb -E LATIN1 -O tcontacta -l C -T template0 tcontacta;'
createlang -U tcontacta -ptcontacta --host=127.0.0.1 --port=5432 plpgsql tcontacta
pg_restore -h 127.0.0.1 -p 5432 -U tcontacta -d "tcontacta" -C -v "/usr/src/1Java_Tomcat/tcontacta.dmp"
/etc/init.d/postgresql restart

rm -rf /etc/odbc.ini
cp /usr/src/2Conexion/odbc.ini /etc/
rm -rf /etc/odbcinst.ini
cp /usr/src/2Conexion/odbcinst.ini /etc/
chmod 777 /etc/odbc.ini
chmod 777 /etc/odbcinst.ini

mv /etc/asterisk/ /etc/bk_asterisk
cp -R /usr/src/2Conexion/asterisk/ /etc/
chmod -R 777 /etc/asterisk/*

cp -R /usr/src/2Conexion/agi-bin/* /var/lib/asterisk/agi-bin/
chmod 777 -R /var/lib/asterisk/agi-bin/*

chmod -R 777 /usr/src/2Conexion/html/*
cp -R /usr/src/2Conexion/html/* /var/www/html/

rm -rf /var/lib/asterisk/sounds/
cp -R /usr/src/2Conexion/sounds/ /var/lib/asterisk/sounds/

cp -R /usr/src/2Conexion/usr-local-sbin/* /usr/local/sbin/
chmod +x /usr/local/sbin/*

cp -R /usr/src/2Conexion/moh/* /var/lib/asterisk/moh/
mkdir /var/lib/asterisk/ami
cp -R /usr/src/2Conexion/ami/* /var/lib/asterisk/ami/
chmod 777 -R /var/lib/asterisk/*

echo "##############"
echo "# Editar IPs #"
echo "##############"

php -q /usr/src/2Conexion/reemplazar_ip.php

rm -rf /etc/php.ini
cp /usr/src/2Conexion/php.ini /etc/

/etc/init.d/ntpd start
chkconfig ntpd on
echo 'hwclock --set --date="2017-05-06 22:00" && date --set="2017-05-06 22:00"'

rm -rf /etc/rc.local
cp /usr/src/2Conexion/rc.local /etc/
php -q /usr/src/2Conexion/reemplazar_pghba.php
/etc/init.d/postgresql restart

php -q /usr/src/0Dependencias/set_dhcpd.php >> /etc/dhcp/dhcpd.conf
/etc/init.d/dhcpd restart
php -q /usr/src/0Dependencias/set_crontab.php >> /etc/crontab
/etc/init.d/crond restart
cp /usr/src/0Dependencias/bkp_tcontacta.php /usr/src/
chmod 750 /usr/src/bkp_tcontacta.php

echo "##################################"
echo "# Por favor reinicie el servidor #"
echo "##################################"


